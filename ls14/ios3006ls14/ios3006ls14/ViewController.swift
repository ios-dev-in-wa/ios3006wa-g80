//
//  ViewController.swift
//  ios3006ls14
//
//  Created by WA on 18.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import AVKit
import Contacts

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private let contactService = ContactService()
    private var contacts: [CNContact] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
//        AVCaptureDevice.requestAccess(for: .video) { isGranted in
//            print(isGranted)
//        }
//        contactService.requestAccess()
        updateContacts()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        print(Settings.shared.isSoundEnabled, Settings.shared.currentSound, Settings.shared.nameValue, Settings.shared.sliderValue)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showContactDetails" {
            guard let controller = segue.destination as? ContactDetailsViewController,
            let indexPath = sender as? IndexPath else { return }
            controller.selectedContact = contacts[indexPath.row]
        }
    }
    
    private func updateContacts() {
        contactService.fetchContacts { [weak self] contacts in
            self?.contacts = contacts
            self?.tableView.reloadData()
        }
    }

    @IBAction func addNewContactAction(_ sender: UIBarButtonItem) {
        contactService.addNewContact()
        updateContacts()
    }

    @IBAction func showSettingsAction(_ sender: UIBarButtonItem) {
        print(Settings.shared.isSoundEnabled, Settings.shared.currentSound, Settings.shared.nameValue, Settings.shared.sliderValue)
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showContactDetails", sender: indexPath)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as? ContactTableViewCell else { fatalError() }
        cell.setupWithContact(contacts[indexPath.row])
        return cell
    }
    
    
}
