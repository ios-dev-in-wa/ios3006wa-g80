//
//  Settings.swift
//  ios3006ls14
//
//  Created by WA on 18.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation


final class Settings {

    static let shared = Settings()

    private let defaults = UserDefaults.standard

    var currentSound: String? {
        get {
            return defaults.string(forKey: "sound_check")
        }
        set {
            
        }
    }

    var nameValue: String {
        get {
            return defaults.string(forKey: "name_preference") ?? ""
        }
        set {
            
        }
    }

    var isSoundEnabled: Bool {
        get {
            return defaults.bool(forKey: "enabled_preference")
        }
    }

    var sliderValue: Double {
        get {
            return defaults.double(forKey: "slider_preference")
        }
    }
}
