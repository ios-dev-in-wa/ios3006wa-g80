//
//  ContactDetailsViewController.swift
//  ios3006ls14
//
//  Created by WA on 18.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Contacts

class ContactDetailsViewController: UIViewController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!

    var selectedContact: CNContact?

    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    private func updateUI() {
        guard let contact = selectedContact else { return }
        if let imageData = contact.thumbnailImageData, let image = UIImage(data: imageData) {
            avatarImageView.image = image
        }
        nameLabel.text = contact.givenName
//        noteLabel.text = contact.note
        jobTitleLabel.text = contact.jobTitle
        phoneNumberLabel.text = contact.phoneNumbers.first?.value.stringValue
    }
}
