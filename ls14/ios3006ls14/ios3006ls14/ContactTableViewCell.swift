//
//  ContactTableViewCell.swift
//  ios3006ls14
//
//  Created by WA on 18.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Contacts

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!

    func setupWithContact(_ contact: CNContact) {
        if let data = contact.thumbnailImageData, let image = UIImage(data: data) {
            avatarImageView.image = image
        }
        nameLabel.text = contact.givenName + " " + contact.familyName
        phoneNumberLabel.text = contact.phoneNumbers.first?.value.stringValue
    }
}
