//
//  ContactsService.swift
//  ios3006ls14
//
//  Created by WA on 18.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Contacts

struct ContactService {
    
    private let contactsStore = CNContactStore()

    func requestAccess() {
        contactsStore.requestAccess(for: .contacts) { (_, _) in
            
        }
    }

    func fetchContacts(completion: (([CNContact]) -> Void)?) {
        var contacts = [CNContact]()
        let keys: [Any] = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey,
            CNContactImageDataAvailableKey,
            CNContactJobTitleKey]

        let predicate = CNContact.predicateForContactsInContainer(withIdentifier: contactsStore.defaultContainerIdentifier())
        do {
            contacts = try contactsStore.unifiedContacts(matching: predicate, keysToFetch: keys as! [CNKeyDescriptor])// [CNContact]
        } catch {
            print(error.localizedDescription)
        }
        completion?(contacts)
    }

    func addNewContact() {
        let newContact = CNMutableContact()
        newContact.givenName = "Your Name"
        newContact.jobTitle = "CTO xyz Company"
//
        let workEmail = CNLabeledValue(label: CNLabelWork, value: "demoxyz@gmail.com" as NSString)
        newContact.emailAddresses = [workEmail]
        newContact.phoneNumbers = [CNLabeledValue(
            label:CNLabelPhoneNumberiPhone,
            value:CNPhoneNumber(stringValue:"0123456789"))]
        do {
            let saveRequest = CNSaveRequest()
            saveRequest.add(newContact, toContainerWithIdentifier: nil)
            try contactsStore.execute(saveRequest)
        } catch {
            print(error.localizedDescription)
        }
    }
}
