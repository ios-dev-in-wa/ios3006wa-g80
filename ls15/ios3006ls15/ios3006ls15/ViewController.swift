//
//  ViewController.swift
//  ios3006ls15
//
//  Created by WA on 20.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

enum MathAction: String {
    case multiply = "*"
    case divide = "/"
    case increment = "+"
    case subsctract = "-"
}

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!

    var firstNumber: String?
    var currentMathAction: MathAction?
    var isNeedToMoveToNextNumber = false

    @IBAction func numberAction(_ sender: UIButton) {
        if resultLabel.text == "0" {
            resultLabel.text = ""
        }
        if isNeedToMoveToNextNumber {
            resultLabel.text = ""
            isNeedToMoveToNextNumber = false
        }
        resultLabel.text! += sender.currentTitle ?? ""
    }

    @IBAction func addMathAction(_ sender: UIButton) {
        if firstNumber == nil {
            firstNumber = resultLabel.text
        }
        if let title = sender.currentTitle {
            currentMathAction = MathAction(rawValue: title)
        }
        isNeedToMoveToNextNumber = true
    }
    
    @IBAction func calculateAction(_ sender: UIButton) {
        guard let secondNumber = Double(resultLabel.text!), let firstNumber = firstNumber, let number = Double(firstNumber) else { return }
        var result = "Error in math"
        switch currentMathAction {
        case .divide:
            let resultValue = number / secondNumber
            if (resultValue.truncatingRemainder(dividingBy: 1.0) == 0) {
                result = "\(Int(resultValue))"
            } else {
                result = "\(resultValue)"
            }
        default:
            break
        }
        currentMathAction = nil
        self.firstNumber = nil
        resultLabel.text = result
    }
}

