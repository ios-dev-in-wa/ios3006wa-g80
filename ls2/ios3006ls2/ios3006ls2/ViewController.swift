//
//  ViewController.swift
//  ios3006ls2
//
//  Created by WA on 02.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

//import Foundation // Base types and arifmetics
import UIKit // All things with UI

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        printTest()
//        arifmetica()
        // ==
        // <=
        // >=
//        // !=
//        let isEqual = squareOfNumbers(number: 10) == 100
//        if isEqual {
//            print("FUNCTION WORKS CORRECTLY")
//        }
//        print(squareOfNumbers())
//        print(squareOfNumbers(number: 200))
//        counterExample(countTill: 10)
//        greeting(name: "Artem", times: 10)
//        greeting(name: "Ilya", times: 2)
//
//        yearSwitchExample(year: 2020)
//        yearSwitchExample(year: 2025)
        view.backgroundColor = UIColor.blue
//        addBox()
//        addBox(x: 0)
//        addBox(y: 200)
        solveFirstPuzzle(times: 4)
    }

    func solveFirstPuzzle(times: Int) {
        var counter = 0
        for _ in 0..<times {
            let spacing = 10
            let width = 100
            let newX: Double = Double(width * counter + spacing * counter)
            let newY = 20.0
            addBox(x: newX, y: newY)
            counter += 1
        }
    }

    func addBox(x: Double = 100, y: Double = 100) {
        let box = UIView()
        box.frame = CGRect(x: x, y: y, width: 100, height: 100)
        box.backgroundColor = UIColor.black
        view.addSubview(box)
    }

    func printTest() {
        var name = "Artem"
        print(name)
        name = "Valera"
        print(name)

//        var nilContainer: Any = 12312

        var year = 2010.2020
        let stringYear = String(2020)
        print(stringYear)

        if let newYear = stringYear as? Int {
            print(newYear)
        } else {
            print("VALUE IS NIL")
        }

        var isWinterComming = true
        isWinterComming = false

        if isWinterComming {
            
        } else {
            
        }
    }

    func arifmetica() {
        let sum = 2 + 2
        let multiply = 2 * 2
        let divide = 2 / 2
        let vicitanie = 2 - 2
        let ostatok = 2 % 2
        let stepen = 2 ^ 2
        let modul = abs(divide)
        let pi = Double.pi
        let sq = sqrt(pi)
        print(sum, multiply, divide, vicitanie)
    }

    func squareOfNumbers(number: Int = 2) -> Int {
//        let number = 8
        let square = number * number
        return square
    }

    func multiplyNumbers(numberOne: Int, numberTwo: Int) -> Int {
        let result = numberOne * numberTwo
        return result
    }

    func sqrNumber(number: Int, count: Int) -> Int {
        var result = number
        for _ in 0..<count {
            result *= number
//            += -= /= *= %=
        }
        return result
    }

    func counterExample(countTill: Int) {
        var counter = 0
        for _ in 0..<countTill {
            counter += 1
            print(counter)
        }
    }

    func greeting(name: String, times: Int) {
        for _ in 0..<times {
            print("Greeetings to you, \(name)")
        }
    }

    func yearSwitchExample(year: Int) {
        switch year {
        case 2020:
            print("YO! YOu are living now")
        case 2007:
            print("YO! YOu are living great time")
        default:
            print(year, "Is out of scope")
        }
    }

    func switchBoolExample(x: Int, y: Int) -> String {
        let result = x > y
        switch result {
        case true: return "X is bigger then Y"
        case false: return "Y is bigger then X"
        }
    }
}

