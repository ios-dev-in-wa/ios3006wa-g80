//
//  DrawableView.swift
//  ios3006ls10
//
//  Created by WA on 04.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class DrawableView: UIView {

    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()

        path.move(to: CGPoint(x: 200, y: 200))
        path.addLine(to: .init(x: 200, y: 400))
        path.addLine(to: .init(x: 300, y: 400))
        path.addLine(to: .init(x: 200, y: 200))

        path.close()

        path.lineWidth = 2

        UIColor.systemGreen.setFill()
        UIColor.systemRed.setStroke()

        path.stroke()
        path.fill()
    }

}
