//
//  AlertView.swift
//  ios3006ls10
//
//  Created by WA on 04.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

enum AlertType {
    case normal
    case destructive

    var color: UIColor {
        switch self {
        case .normal: return .systemGreen
        case .destructive: return .systemRed
        }
    }
}

struct AlertViewModel {
    let alertType: AlertType
    let title: String
    let subtitle: String?
    let buttonTitle: String
}

class AlertView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!

    private var buttonClosure: (() -> Void)?

    func setupWith(model: AlertViewModel, action: @escaping (() -> Void)) {
//        statusImageView.image // isPositive
        titleLabel.text = model.title
        if let subtitle = model.subtitle {
            subtitleLabel.isHidden = false
            subtitleLabel.text = subtitle
        } else {
            subtitleLabel.isHidden = true
        }
        mainButton.setTitle(model.buttonTitle, for: .normal)
        mainButton.backgroundColor = model.alertType.color
        buttonClosure = action
    }

    @IBAction func buttonAction(_ sender: UIButton) {
        buttonClosure?()
    }

    @IBAction func outsideTapAction(_ sender: UITapGestureRecognizer) {
        if !containerView.frame.contains(sender.location(in: self)) {
            removeFromSuperview()
        }
    }
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        removeFromSuperview()
//    }
}
