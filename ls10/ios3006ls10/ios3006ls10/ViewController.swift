//
//  ViewController.swift
//  ios3006ls10
//
//  Created by WA on 04.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var users: [User] = []
    private let service = UserService()
    private var currentPage = 1
    private var isLoadInProgress = false
    private var isListCompleted = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self

        let nib = UINib(nibName: "ProfileTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ProfileTableViewCell")
        loadMore()
    }

    func loadMore() {
        guard !isLoadInProgress, !isListCompleted else { return }
        isLoadInProgress = true
        service.getUsers(page: currentPage) { [weak self] users in
            if users.count == 0 {
                self?.isListCompleted = true
            }
            self?.isLoadInProgress = false
            self?.users += users
            self?.currentPage += 1
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let drawable = DrawableView()

        drawable.frame = .init(x: 20, y: 20, width: 100, height: 100)

        view.addSubview(drawable)
    }

    @IBAction func showAlert(_ sender: UIBarButtonItem) {
//        let alertView: AlertView = .fromNib()
//        alertView.frame = UIScreen.main.bounds
//        navigationController?.view.addSubview(alertView)
//        alertView.setupWith(model: .init(alertType: .destructive, title: "WOw, take it easy", subtitle: nil, buttonTitle: "Take easy!"), action: {
//            alertView.removeFromSuperview()
//        })
        let alertController = UIAlertController(title: "Warning", message: "You speed is above 10000", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { action in
            print("WHOOPS")
        }))
        alertController.addAction(UIAlertAction(title: "notOK", style: .default, handler: { action in
            print("WHOOPS")
        }))
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as? ProfileTableViewCell else {
            fatalError()
        }
        if indexPath.row == users.count - 5 {
            loadMore()
        }
        cell.setupWith(user: users[indexPath.row])
        return cell
    }
    
    
}
