//
//  UIView+fromNib.swift
//  ios3006ls10
//
//  Created by WA on 04.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
