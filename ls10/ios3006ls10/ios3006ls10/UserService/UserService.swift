//
//  UserService.swift
//  ios3006ls10
//
//  Created by WA on 04.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct UserService {

    private let session = URLSession.shared
    private let mainUrl = "https://randomuser.me/api/"

    func getUsers(page: Int, count: Int = 20, successBlock: (([User]) -> Void)?) {
        guard let url = URL(string: mainUrl + "?page=\(page)&results=\(count)") else { return }
//        "https://randomuser.me/api/?results=10"
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200:
                guard let data = data else { return }
                do {
                    let dataResponse = try JSONDecoder().decode(UsersResponse.self, from: data)
                    successBlock?(dataResponse.results)
                } catch {
                    print(error.localizedDescription)
                }
            default:
                print("UNEXPECTED CODE")
            }
        }.resume()
    }
}
