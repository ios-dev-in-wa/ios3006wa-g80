//
//  ProfileTableViewCell.swift
//  ios3006ls10
//
//  Created by WA on 04.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    func setupWith(user: User) {
        if let url = URL(string: user.picture.large) {
            profileImageView.kf.indicatorType = .activity
            profileImageView.kf.setImage(with: url)
        }
        nameLabel.text = user.name.fullName
        emailLabel.text = user.email
    }
}
