//
//  Car.swift
//  ios3006ls11
//
//  Created by WA on 06.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse

// MARK: - Car
struct Car: Codable {
    
    let model: String
    let wasInAccident: Bool
    let productionYear: Int?
    let objectId: String

    init(model: String, wasInAccident: Bool, productionYear: Int?, objectId: String) {
        self.model = model
        self.wasInAccident = wasInAccident
        self.productionYear = productionYear
        self.objectId = objectId
    }

    init?(object: PFObject) {
        guard let model = object[CodingKeys.model.stringValue] as? String,
            let objectId = object.objectId else { return nil }
            
        self.model = model
        self.wasInAccident = object[CodingKeys.wasInAccident.stringValue] as? Bool ?? false
        self.productionYear = object[CodingKeys.productionYear.stringValue] as? Int
        self.objectId = objectId
    }

    var object: PFObject {
        let object = PFObject(className: "Car")
        object[CodingKeys.model.stringValue] = model
        object[CodingKeys.wasInAccident.stringValue] = wasInAccident
        object[CodingKeys.productionYear.stringValue] = productionYear
        return object
    }
}
