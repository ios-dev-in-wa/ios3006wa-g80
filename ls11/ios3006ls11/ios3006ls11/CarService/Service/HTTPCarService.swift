//
//  HTTPCarService.swift
//  ios3006ls11
//
//  Created by WA on 11.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct HTTPCarService: CarServiceProtocol {

    private let mainURL = URL(string: "https://parseapi.back4app.com/classes/Car")!

    private let session = URLSession.shared

    private func setupRequest(_ request: inout URLRequest) {
        //        X-Parse-Application-Id: ZEhpVBbX4Y3Q152oKKd1jdo1aoU25dvIOgHRXP6v
        //
        //        X-Parse-REST-API-Key: SC1kd7OHF8Inggx3EV40xnRq8Ki5kSIJY7VSyy7L
        //
        //        Content-Type: application/json
        request.addValue("ZEhpVBbX4Y3Q152oKKd1jdo1aoU25dvIOgHRXP6v", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("SC1kd7OHF8Inggx3EV40xnRq8Ki5kSIJY7VSyy7L", forHTTPHeaderField: "X-Parse-REST-API-Key")
    }
    
    func createCar(car: Car, completion: ((Car, Bool) -> Void)?) {
        var request = URLRequest(url: mainURL)
        request.httpMethod = "POST"
        setupRequest(&request)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            let data = try JSONEncoder().encode(CarRequest(car: car))
            request.httpBody = data

            session.dataTask(with: request) { (responseData, response, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                guard let data = responseData else { return }
                do {
                    let createCarResponse = try JSONDecoder().decode(CreateCarResponse.self, from: data)
                    self.getCar(carId: createCarResponse.objectId) { car in
                        DispatchQueue.main.async {
                            completion?(car, true)
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }.resume()
        } catch {
            print(error.localizedDescription)
        }
    }

    func getCar(carId: String, completion: ((Car) -> Void)?) {
        var request = URLRequest(url: mainURL.appendingPathComponent(carId))
        request.httpMethod = "GET"
        setupRequest(&request)

        session.dataTask(with: request) { (responseData, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = responseData else { return }
            do {
                let createCarResponse = try JSONDecoder().decode(Car.self, from: data)
                completion?(createCarResponse)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func readCars(closure: (([Car]) -> Void)?) {
        var request = URLRequest(url: mainURL)
        request.httpMethod = "GET"
        setupRequest(&request)

        session.dataTask(with: request) { (responseData, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = responseData else { return }
            do {
                let createCarResponse = try JSONDecoder().decode(ReadCarsResponse.self, from: data)
                DispatchQueue.main.async {
                    closure?(createCarResponse.results)
                }
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func updateCar(car: Car, completion: ((Car) -> Void)?) {
        var request = URLRequest(url: mainURL.appendingPathComponent(car.objectId))
        request.httpMethod = "PUT"
        setupRequest(&request)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            let data = try JSONEncoder().encode(CarRequest(car: car))
            request.httpBody = data
            
            session.dataTask(with: request) { (responseData, response, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { return }
                self.getCar(carId: car.objectId) { car in
                    DispatchQueue.main.async {
                        completion?(car)
                    }
                }
            }.resume()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteCar(car: Car, completion: (() -> Void)?) {
        var request = URLRequest(url: mainURL.appendingPathComponent(car.objectId))
        request.httpMethod = "DELETE"
        setupRequest(&request)

        session.dataTask(with: request) { (responseData, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { return }
            DispatchQueue.main.async {
                completion?()
            }
        }.resume()
    }
}
