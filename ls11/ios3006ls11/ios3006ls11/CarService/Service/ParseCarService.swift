//
//  ParseCarService.swift
//  ios3006ls11
//
//  Created by WA on 06.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse

struct ParseCarService: CarServiceProtocol {
    func getCar(carId: String, completion: ((Car) -> Void)?) {
        
    }

    private let query = PFQuery(className: "Car")

    func createCar(car: Car, completion: ((Car, Bool) -> Void)?) {
        let carObject = car.object
        carObject.saveInBackground { (success, error) in
           if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let car = Car(object: carObject) else { return }
            completion?(car, success)
        }
    }
    
    func readCars(closure: (([Car]) -> Void)?) {
        query.findObjectsInBackground { (objects, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let objects = objects else { return }
            let cars = objects.compactMap { Car(object: $0) }
            closure?(cars)
        }
    }
    
    func updateCar(car: Car, completion: ((Car) -> Void)?) {
        query.getObjectInBackground(withId: car.objectId) { object, error in
            guard let object = object else {
                print("Object with id: \(car.objectId) doesn't exist")
                return
            }
            object.updateWithCar(car: car)
            object.saveInBackground { (success, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                if success {
                    guard let car = Car(object: object) else { return }
                    completion?(car)
                } else {
                    print("ERROR while saving udpate car")
                }
            }
        }
    }

    func deleteCar(car: Car, completion: (() -> Void)?) {
        query.getObjectInBackground(withId: car.objectId) { object, error in
            guard let object = object else {
                print("Object with id: \(car.objectId) doesn't exist")
                return
            }
            object.deleteInBackground { (success, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                if success {
                    completion?()
                } else {
                    print("ERROR while deleting car")
                }
            }
        }
    }
}

extension PFObject {
    func updateWithCar(car: Car) {
        self["model"] = car.model
        self["wasInAccident"] = car.wasInAccident
        self["productionYear"] = car.productionYear
    }
}
