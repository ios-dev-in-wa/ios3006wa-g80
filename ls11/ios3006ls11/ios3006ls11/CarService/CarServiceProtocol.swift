//
//  CarServiceProtocol.swift
//  ios3006ls11
//
//  Created by WA on 06.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

protocol CarServiceProtocol {
    func createCar(car: Car, completion: ((Car, Bool) -> Void)?)
    func readCars(closure: (([Car]) -> Void)?)
    func getCar(carId: String, completion: ((Car) -> Void)?)
    func updateCar(car: Car, completion: ((Car) -> Void)?)
    func deleteCar(car: Car, completion: (() -> Void)?)
}
