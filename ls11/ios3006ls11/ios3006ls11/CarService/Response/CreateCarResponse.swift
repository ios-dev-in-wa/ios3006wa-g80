//
//  CarResponse.swift
//  ios3006ls11
//
//  Created by WA on 11.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct CreateCarResponse: Codable {
    let objectId: String
    let createdAt: String
}
