//
//  ReadCarsResponse.swift
//  ios3006ls11
//
//  Created by WA on 11.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct ReadCarsResponse: Codable {
    let results: [Car]
}
