//
//  CarRequest.swift
//  ios3006ls11
//
//  Created by WA on 11.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct CarRequest: Codable {
    let model: String
    let wasInAccident: Bool
    let productionYear: Int?

    init(car: Car) {
        model = car.model
        wasInAccident = car.wasInAccident
        productionYear = car.productionYear
    }
}
