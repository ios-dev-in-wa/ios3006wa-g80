//
//  MasterViewController.swift
//  ios3006ls11
//
//  Created by WA on 06.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: AddCarViewController? = nil
    var cars = [Car]()
    var carService: CarServiceProtocol {
        return HTTPCarService()//ParseCarService()
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = editButtonItem

        carService.readCars(closure: { [weak self] cars in
            self?.cars = cars
            self?.tableView.reloadData()
        })
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? AddCarViewController
            detailViewController?.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            detailViewController?.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    private func saveCar(car: Car, isInEdit: Bool) {
        if isInEdit {
            carService.updateCar(car: car, completion: { [weak self] car in
                if let index = self?.cars.firstIndex(where: { $0.objectId == car.objectId}) {
                    self?.cars.remove(at: index)
                    self?.cars.insert(car, at: index)
                    self?.tableView.reloadData()
                }
            })
        } else {
            carService.createCar(car: car) { [weak self] car, success in
                if success {
                    self?.cars.append(car)
                    self?.tableView.reloadData()
                    print("Successfully saved new Car")
                } else {
                    print("Unexpected error")
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            guard let navVC = segue.destination as? UINavigationController,
                let destinationVC = navVC.topViewController as? AddCarViewController else { return }
            detailViewController = destinationVC
            detailViewController?.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            detailViewController?.navigationItem.leftItemsSupplementBackButton = true
            if let indexPath = tableView.indexPathForSelectedRow {
                destinationVC.car = cars[indexPath.row]
            }
            destinationVC.isInEdit = false
            destinationVC.didPressSave = saveCar
        }
    }

    @IBAction func showAddNewCarVC(_ sender: UIBarButtonItem) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "AddCarViewController") as? AddCarViewController else { return }
        vc.didPressSave = saveCar
        present(vc, animated: true, completion: nil)
//        performSegue(withIdentifier: "showDetail", sender: nil)
    }
    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let object = cars[indexPath.row]
        cell.textLabel?.text = object.model
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: nil)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            carService.deleteCar(car: cars[indexPath.row]) { [weak self] in
                self?.cars.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

