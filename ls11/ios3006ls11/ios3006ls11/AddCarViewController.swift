//
//  AddCarViewController.swift
//  ios3006ls11
//
//  Created by WA on 06.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class AddCarViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var carModelTextField: UITextField!
    @IBOutlet weak var productionYearTextField: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    
    var didPressSave: ((Car, Bool) -> Void)?
    var didPressSaveEdited: ((Car) -> Void)?
    var car: Car?
    var isInEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        observeKeyboard()
        updateUI()
        if !isInEdit, car != nil {
            let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonAction))
            navigationItem.rightBarButtonItem = editButton
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    private func observeKeyboard() {
//        UIDevice <- notifications
        NotificationCenter.default.addObserver(self, selector: #selector(manageKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(manageKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func manageKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = .zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }

        guard let textField = [carModelTextField, productionYearTextField].compactMap({ $0 }).first(where: { $0?.isFirstResponder == true }) else { return }
        scrollView.scrollRectToVisible(textField.frame, animated: true)
    }

    private func updateUI() {
        if let car = car {
            carModelTextField.isEnabled = isInEdit
            carModelTextField.text = car.model
            wasInAccidentSwitch.isEnabled = isInEdit
            wasInAccidentSwitch.isOn = car.wasInAccident
            if let year = car.productionYear {
                productionYearTextField.text = String(year)
            }
            productionYearTextField.isEnabled = isInEdit
            saveButton.isHidden = !isInEdit
        }
    }

    @objc func editButtonAction() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "AddCarViewController") as? AddCarViewController else { return }
        vc.car = car
        vc.didPressSaveEdited = { [weak self] car in
            self?.car = car
            self?.updateUI()
            self?.didPressSave?(car, true)
        }
        vc.isInEdit = true
        present(vc, animated: true, completion: nil)
    }
  
    @IBAction func saveChangesAction(_ sender: UIButton) {
        UIView.transition(with: self.view, duration: 0.5, options: [.transitionFlipFromLeft], animations: {
            self.carModelTextField.text = "Car model name"
            self.productionYearTextField.text = "Car production year"
        }, completion: nil)
//        guard let model = carModelTextField.text else { return }
//        let car = Car(model: model, wasInAccident: wasInAccidentSwitch.isOn, productionYear: Int(productionYearTextField.text ?? ""), objectId: self.car?.objectId ?? UUID().uuidString)
//        if isInEdit {
//            didPressSaveEdited?(car)
//        } else {
//            didPressSave?(car, isInEdit)
//        }
//        if presentingViewController != nil {
//            dismiss(animated: true, completion: nil)
//        }
    }
}
