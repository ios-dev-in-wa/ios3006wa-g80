//
//  AnimationsViewController.swift
//  ios3006ls11
//
//  Created by WA on 11.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class AnimationsViewController: UIViewController {

    var box = UIView(frame: .init(x: 50, y: 50, width: 100, height: 100)) {
        willSet {
            
        }
        didSet {
           print("Value changed")
        }
    }

    var observer: NSKeyValueObservation?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(box)
        box.backgroundColor = .black
        observer = view.observe(\UIView.backgroundColor) { (view, value) in
            print("VIEW background color did changed")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animate(withDuration: 5) {
            self.box.backgroundColor = .cyan
            self.box.frame = CGRect(x: 100, y: 500, width: 10, height: 60)
            self.box.layer.cornerRadius = self.box.frame.height / 2
            self.box.center = self.view.center
            self.box.transform = .init(scaleX: 2, y: 0.5)
            self.box.transform = .init(rotationAngle: 100)
            self.view.backgroundColor = .black
        }

//        let animator = UIViewPropertyAnimator(duration:0.3, curve: .linear) {
//            view.frame = view.frame.offsetBy(dx:100, dy:0)
//        }
//        animator.fractionComplete
        UIView.animate(withDuration: 2, delay: 0.1, options: [.autoreverse, .repeat, .curveEaseOut], animations: {
            self.box.backgroundColor = .cyan
            self.box.frame = CGRect(x: 100, y: 500, width: 10, height: 60)
        }, completion: { isFinished in
            print(isFinished)
        })

//        UIView.transition(with: box, duration: 4, options: [.transitionCurlUp], animations: {
//            self.box.backgroundColor = .cyan
//        }, completion: nil)
        
    }
}
