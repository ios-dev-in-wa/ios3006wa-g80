//
//  ConnectionType.swift
//  ios3006ls4
//
//  Created by WA on 09.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

enum ConnectionType {
    case edge(String)
    case threeG
    case fourG
    case five5
}

class Phone {
    var connectionType: ConnectionType = .five5

    func changeToGSM() {
        connectionType = .five5
        switch connectionType {
        case .edge(let value):
//            value
            print("EDGE")
        case .five5:
            print("5G")
        default:
            break
        }
    }
}
