//
//  ViewController.swift
//  ios3006ls4
//
//  Created by WA on 09.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    weak var robotDefault: Robot?
    let battleRobotOne = BattleRobot()
    let battleRobotTwo = BattleRobot(health: 1000.0, name: "C3-PO-Husler")

    override func viewDidLoad() {
        super.viewDidLoad()
//        let isValidData = isEditing && isViewLoaded
//        if isValidData {
//
//        }
        let customRobot = Robot(health: 75.0, name: "Robotic hand")
        robotDefault = customRobot
        battleRobotOne.friend = customRobot
        battleRobotTwo.move()
//        Robot.version
//        BattleRobot.version
//        print(robotDefault.name, customRobot.name)
//        foo()
//        battleRobotOne.beHappyRobot()
//        while battleRobotOne.health > 0, battleRobotTwo.health > 0 {
//            makeFight(firstRobot: battleRobotOne, secondRobot: battleRobotTwo)
//        }
//        arcExample()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            print(self.robotDefault?.name)
//        }

//        print(isNaOrIch(otchestvo: "Olegovna"), isNaOrIch(otchestvo: "Olegovich"), isNaOrIch(otchestvo: "Kimoni"))
//        uppercaseSearch(value: "IvanVasilievich")
//        reversedString(value: "Artem")
        commas(value: "1234")
        prob(val: "13456789")
    }

    func arcExample() {
//        robotDefault = nil
        print(battleRobotOne.friend?.name)
//        robotDefault = nil
        battleRobotOne.friend = nil
        print(battleRobotOne.friend?.name, robotDefault?.name)
    }

    func foo() {
        let battleRobotOne = BattleRobot()
        print(battleRobotOne.name, battleRobotOne.newName)
        print(battleRobotOne.setName(name: "C3-PO.Robot"))
        print(battleRobotOne.name, battleRobotOne.newName)
        battleRobotOne.move()
    }

    func makeFight(firstRobot: BattleRobot, secondRobot: BattleRobot) {
        let firstDmg = firstRobot.attack()
        let secondDmg = secondRobot.attack()
        firstRobot.getDamage(damage: firstDmg)
        secondRobot.getDamage(damage: secondDmg)
    }

    func isNaOrIch(otchestvo: String) -> Bool {
        return otchestvo.hasSuffix("ich") || otchestvo.hasSuffix("na")
    }

    // "IvanVasilievich"
    func uppercaseSearch(value: String) {
        print(value)
        var name: String = ""
        var surname = ""
        if let char = value.last { char -> Bool in return char.isUppercase } {
            let replaced = value.replacingOccurrences(of: "\(char)", with: " \(char)")
            let splited = replaced.split(separator: " ")
            name = String(splited.first!)
            surname = String(splited.last!)
        }
        print(name, surname)
    }

    func reversedString(value: String) {
        var newValue = ""
        for char in value {
            newValue.insert(char, at: newValue.startIndex)
        }
        print(newValue)
    }

    func commas(value: String) {
        var result = ""
        var counter = 0
        for char in value.reversed() {
            result += String(char)
            if counter == 2 {
                result += ","
                counter = 0
            } else {
                counter += 1
            }
        }
        print(result.reversed())
    }

    func prob(val: String) {
        var newVal = ""
        let id = val.count % 3
        var i = 0
        for char in val {
            newVal.append(char)
            i += 1
            if i % 3 == id && i != (val.count) {
                newVal.append(",")
            }
        }
        print(newVal)
    }
}

extension String {
    subscript (index: Int) -> Character {
        let charIndex = self.index(self.startIndex, offsetBy: index)
        return self[charIndex]
    }

    subscript (range: Range<Int>) -> Substring {
        let startIndex = self.index(self.startIndex, offsetBy: range.startIndex)
        let stopIndex = self.index(self.startIndex, offsetBy: range.startIndex + range.count)
        return self[startIndex..<stopIndex]
    }

}
