//
//  Robot.swift
//  ios3006ls4
//
//  Created by WA on 09.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Robot {
    var health: Double// = 100.0
    private(set) var name: String// = "Mr.Robot"

    static let version = 1.0

    var friend: Robot?

    init() {
        health = 100.0
        name = "Mr.Robot"
    }

    init(health: Double, name: String) {
        self.health = health
        self.name = name
    }

    func move() {
        print("\(name) is moving")
    }

    func beHappyRobot() {
        print("\(name) so happy")
    }

    func setName(name: String) -> String {
        guard !name.isEmpty else {
            return "Robot name cannot be empty"
        }
        guard name.count > 4 else {
            return "Robot name must be longer than 4 symbols"
        }
        guard name.contains("Robot") else {
            return "Robot name must contain 'Robot'"
        }

        self.name = name
        return "Name is changed!"
    }
}
