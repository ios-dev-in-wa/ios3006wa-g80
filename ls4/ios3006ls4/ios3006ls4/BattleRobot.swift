//
//  BattleRobot.swift
//  ios3006ls4
//
//  Created by WA on 09.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class BattleRobot: Robot {

    var newName: String {
        return name + "Battler"
    }

    override init() {
        super.init()
        print(setName(name: "DROIDEKA"))
        health = 1000.0
    }

    override init(health: Double, name: String) {
        super.init(health: health, name: name)
    }

    override func move() {
//        super.move()
        print("\(name) is moving with agression!!!!")
    }

    func attack() -> Double {
        return Double.random(in: 0.0...10.0).rounded(toPlaces: 2)
    }

    func getDamage(damage: Double) {
        health -= damage
        print("\(name) get damage: \(damage), his current HP:\(health)")
    }
}
