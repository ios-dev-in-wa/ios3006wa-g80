//
//  Double+rounded.swift
//  ios3006ls4
//
//  Created by WA on 09.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
