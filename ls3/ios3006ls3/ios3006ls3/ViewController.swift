//
// ViewController.swift
// SelfTaskKadyrov
//
// Created by Kadir Kadyrov on 07.07.2020.
// Copyright © 2020 Kadir Kadyrov. All rights reserved.
//
import UIKit
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

//        greeting(name: "Artem")
//
//        print("Hello world")
//        intExample()
//        doubleExample(val: 2)
//        stringExample()
//        arrayExample()
//        dictionaryExample()
        setExample()
    }

    func greeting(name: String) {
        print("Hello, \(name)")
    }

    func intExample() {
        print(Int.max, Int.min)
        for _ in 0...10 {
            print(Int.random(in: 0...10))
        }
//        let someInt = 1
//        someInt.
    }

    func doubleExample(val: Double) {
//        let some = val - val
//        let val: Double = 0 / some
    }

    func stringExample() {
        var helloWorld = "Hello world, Hello"
        helloWorld.append("Ssssss")
        print(helloWorld, helloWorld.count, helloWorld.lowercased())
        print(helloWorld.contains("H"), helloWorld.contains("hello"))
        print(helloWorld.replacingOccurrences(of: "Hello", with: "Goodbye!!!"))
    }

    func arrayExample() {
//        let names: [String] = []
        var names = ["Artem", "Ihor", "Sergey"]
//        let years = [2020, 2019, 2018]
        print(names[2])
        names.append("Alla")
        names.append("Artem")
        print(names, names.count)
        if let firstName = names.first {
            print(firstName)
        }
        print(names.firstIndex(of: "Artur"))
        names.insert("Ilya", at: 0)
        print(names)

        print(names.startIndex)
        names.remove(at: Int.random(in: 0...names.count - 1))
        print(names)
        names.shuffle()
        print(names)
        for name in names {
            print(name)
        }
        let sortedNames = names.sorted { (valueOne, valueTwo) -> Bool in
            return valueOne.count < valueTwo.count
        }
        print(sortedNames)

        let filteredNames = names.filter { value -> Bool in
            return value.count == 4
        }

        names.forEach { name in
            print(name)
        }
        print(filteredNames)
//        print(names.startIndex)

        let intArray = names.map { name -> Int in
            return name.count
        }
        print(intArray)

        names.append("1")
        let intArrayTwo = names.compactMap { name -> Int? in
            return Int(name)
        }
        print(intArrayTwo)
    }

//    func sorted(valOne: String, valTwo: String) -> Bool {
//
//    }
    
    func dictionaryExample() {
        let contactBook: [String: String] = [
            "Police": "102",
            "Emergency": "103",
            "Fire fighters": "101"
        ]
        print(contactBook["Police"], contactBook["Mommy"])

        let intDictionary = [
            1: 2.0,
            2: 3.0
        ]

        print(intDictionary[1])

        print(contactBook.keys, contactBook.values)
        var contactBookTwo: [String: [String]] = [
            "Police": ["102", "911"],
            "Emergency": ["103"],
            "Fire fighters": ["101"]
        ]
        print(contactBookTwo.keys, contactBookTwo.values)
        print(contactBookTwo)

        contactBookTwo["Police"] = ["NIL"]
        print(contactBookTwo)

        let test = contactBookTwo.first
        print(test?.key, test?.value)

        contactBookTwo.forEach { (key, value) in
            if key == "Police" || key == "Emergency" {
                value.forEach { number in
                    callToNumber(number: number)
                }
            }
        }
    }

    func callToNumber(number: String) {
        print("Calling to number: \(number)")
    }

    func setExample() {
        let stringSet: Set<String> = ["Artem", "Ihor", "Sergey", "Artem"]
        let stringSetTwo: Set<String> = ["Artem", "Sergey"]
        print(stringSet.intersection(stringSetTwo))
        print(stringSet)
    }
}
