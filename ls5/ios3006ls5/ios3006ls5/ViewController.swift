//
//  ViewController.swift
//  ios3006ls5
//
//  Created by WA on 14.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var yellowView: UIView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var pressMeButton: UIButton!
    
    var valueInt = 0
    var someClosure: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        someClosure = { [weak self] in
            self?.valueInt = 2000
        }

        yellowView.center = view.center
        pressMeButton.setTitle("UPPPS", for: .selected)
    }

    func myFunc() {
        
    }

    @IBAction func pressMeAction(_ sender: UIButton) {
        print("UPS I DID IT AGAIN")
    }
}

