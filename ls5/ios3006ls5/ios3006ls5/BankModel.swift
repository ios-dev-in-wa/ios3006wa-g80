//
//  BankModel.swift
//  ios3006ls5
//
//  Created by WA on 14.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

enum Currency: String {
    case dollar = "$"
    case euro = "E"
}

class BankModel {
    private var balance: Double = 0.0
    private var possibleIncome: Double = 0.0
    private var percent: Double = 1.30
    private var currency: Currency = .dollar

    private var users: [String: String] = [
        "9111" : "Artem"
    ]

    init(balance: Double) {
        self.balance = balance
    }

    func getBalance() -> String {
        return "\(balance) \(currency.rawValue)"
    }

    func authorize(pinCode: String) -> Bool {
        return users[pinCode] != nil
    }

    func getMoney(amount: Double) -> Bool {
        guard balance >= amount else {
            print("Amount is bigger than balance")
            return false
        }
        balance -= amount
        return true
    }

    func calculateTakeMoney(amount: Double) -> Double {
        return amount * percent
    }

    func takeMoney(amount: Double) {
        balance += amount
    }
}
