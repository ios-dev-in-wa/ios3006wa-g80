//
//  CreditViewController.swift
//  ios3006ls5
//
//  Created by WA on 14.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class CreditViewController: UIViewController {

    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var balanceValueLabel: UILabel!
    @IBOutlet weak var bankBalanceValueLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var moneyTextField: UITextField!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var giveBackTextField: UITextField!
    @IBOutlet weak var giveMoneyButton: UIButton!
    
    private let bank = BankModel(balance: 10000.0)
    private var isAutorize = false {
        didSet {
            balanceLabel.isHidden = !isAutorize
            balanceValueLabel.isHidden = !isAutorize

            moneyTextField.placeholder = isAutorize ? "Desired money" : "Pin code"
            moneyTextField.isSecureTextEntry = !isAutorize
            moneyTextField.text = ""

            mainButton.setTitle(isAutorize ? "Get money" : "Enter pincode", for: .normal)
            giveBackTextField.isHidden = !isAutorize
            giveMoneyButton.isHidden = !isAutorize
        }
    }
    private var currentBalance = 10000.0

    override func viewDidLoad() {
        super.viewDidLoad()

        updateBalances()
    }

    func showError(text: String) {
        errorLabel.text = text
        errorLabel.isHidden = false
    }

    func updateBalances() {
        bankBalanceValueLabel.text = bank.getBalance()
        balanceValueLabel.text = "\(currentBalance) $"
    }

    @IBAction func getMoneyAction(_ sender: UIButton) {
        errorLabel.isHidden = true
        if !isAutorize {
            guard let pinCode = moneyTextField.text else { return }
            guard !pinCode.isEmpty else {
                showError(text: "Pin code shouldn't be empty")
                return
            }
            
            isAutorize = bank.authorize(pinCode: pinCode)
            if !isAutorize {
                showError(text: "Error, enter valid pincode")
            }
        } else {
            guard let moneyText = moneyTextField.text else { return }
            guard let amount = Double(moneyText) else { return }
            
            if bank.getMoney(amount: amount) {
                currentBalance += amount
                updateBalances()
            } else {
                showError(text: "NO money, dear")
            }
        }
    }

    @IBAction func giveMoneyAction(_ sender: UIButton) {
        guard let moneyText = giveBackTextField.text else { return }
        guard let amount = Double(moneyText) else { return }
        let calculatedValue = bank.calculateTakeMoney(amount: amount)
        guard currentBalance >= calculatedValue else {
            showError(text: "Not enough money")
            return
        }

        currentBalance -= calculatedValue
        bank.takeMoney(amount: calculatedValue)
        updateBalances()
    }
}
