//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L2H" //  Level name
        super.viewDidLoad()
    }
    
    override func run() {
//        moveFor()
//        moveWhile()
//        solveArtemsPuzzle()
        for _ in 0..<4 {
            solveColumns()
        }
    }

    func solveColumns() {
        turnLeft()
        buildColumn()
        moveToTheNextColumn()
        goToSavePlace()
    }

    func buildColumn() {
        while frontIsClear {
            putCandyIfNeeded()
            move()
        }
        putCandyIfNeeded()
        turnRight()
    }

    func moveToTheNextColumn() {
        if frontIsClear {
            move()
        }
        while leftIsClear, frontIsClear {
            move()
        }
    }

    func putCandyIfNeeded() {
        if noCandyPresent {
            put()
        }
    }

    func goToSavePlace() {
        turnRight()
        while frontIsClear {
            move()
        }
        turnLeft()
    }
    
    func solveArtemsPuzzle() {
        for _ in 0...3 {
            findPeak()
            goToThePeak()
            uTurn()
            moveDown()
        }
    }

    func findPeak() {
        while frontIsClear {
            move()
        }
        turnRight()
    }

    func goToThePeak() {
        while leftIsBlocked {
            move()
        }
    }

    func uTurn() {
        turnLeft()
        move()
        turnLeft()
    }
    
    func moveDown() {
        while frontIsClear {
            move()
        }
        doubleRight()
    }
    
    func doubleRight() {
        turnRight()
        turnRight()
    }

    func moveFor() {
        for _ in 0..<5 {
            move()
        }
        turnRight()
        doPuzzle()
    }

    func moveWhile() {
        while frontIsClear {
            move()
            if candyPresent {
                break
            }
        }
    }

  func solveFirstPuzzle() {
        move()
        doubleMove()
        pick()
        doubleMove()
        turnRight()
        move()
        put()
        turnLeft()
        doubleMove()
    }

    /// This func do double and put candy
    func doPuzzle() {
        doubleMove()
        put()
    }
    
    func doubleMove() {
        saveMove()
        saveMove()
    }

    func saveMove() {
        if frontIsClear && facingRight {
            move()
        } else {
            turnRight()
        }
    }

    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
}
