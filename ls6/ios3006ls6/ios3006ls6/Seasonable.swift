//
//  Season.swift
//  ios3006ls6
//
//  Created by WA on 16.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol Seasonable {
    func winterIsComing()
    func springIsComing()
    func summerIsComing()
    func autumnIsComing()
}

protocol Animal {
    var nick: String { get set }
    var furColor: UIColor { get set }
    var isSleeping: Bool { get set }
}

class Fox: Animal {
    var nick: String
    
    var furColor: UIColor = .orange
    
    var isSleeping: Bool = false

    init(nick: String) {
        self.nick = nick
    }
}

class Rabbit: Animal, Seasonable {
    var nick: String
    
    var furColor: UIColor = .gray
    
    var isSleeping: Bool = false

    init(nick: String) {
        self.nick = nick
    }

    func winterIsComing() {
        print("\(nick) change fuc color to white")
        furColor = .white
    }
    
    func springIsComing() {
        print("\(nick) change fuc color to gray")
        furColor = .gray
    }
    
    func summerIsComing() {
        print("DO nothing")
    }
    
    func autumnIsComing() {
        print("DO nothing")
    }
}

class Bear: Animal, Seasonable {
    var nick: String
    
    var furColor: UIColor = .brown
    
    var isSleeping: Bool = false
    
    init(nick: String) {
        self.nick = nick
    }

    func winterIsComing() {
        isSleeping = true
        print("\(nick) goes to sleep")
    }
    
    func springIsComing() {
        isSleeping = false
        print("\(nick) awakes")
    }
    
    func summerIsComing() {
        
    }
    
    func autumnIsComing() {
        
    }
}
