//
//  ViewController.swift
//  ios3006ls6
//
//  Created by WA on 16.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

/// Delegate
protocol ViewControllerDelegate: class {
    func elementsUpdated(elements: [String])
}

class ViewController: UIViewController {

    @IBOutlet weak var mainButton: UIButton!

    var buttonTitle = "Press me"

    var elements = [String]()
    weak var delegate: ViewControllerDelegate?
    var didUpdateElements: (([String]) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Red", navigationController != nil)
//        navigationItem.titleView = 
        // Do any additional setup after loading the view.
        mainButton.setTitle("\(elements.count) elements", for: .normal)
    }

    func saveAndQuit() {
        if delegate == nil {
            didUpdateElements?(elements)
        } else {
            delegate?.elementsUpdated(elements: elements)
        }
        navigationController?.popViewController(animated: true)
    }

    @IBAction func remoAllAction(_ sender: UIButton) {
        if elements.count >= 1 {
            elements.removeAll()
        }
        saveAndQuit()
    }
    
    @IBAction func removeLastAction(_ sender: UIButton) {
        if elements.count >= 1 {
            elements.removeLast()
        }
        saveAndQuit()
    }
}

