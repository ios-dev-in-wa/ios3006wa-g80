//
//  LoginViewController.swift
//  ios3006ls6
//
//  Created by WA on 16.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    var name: String = ""
    var elements = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("View did Load")
        name = "Artem"
        print("Blue", navigationController != nil)

        let animals: [Animal] = [
            Fox(nick: "Foxy"),
            Rabbit(nick: "Bunny"),
            Rabbit(nick: "Darling"),
            Bear(nick: "Grizly")
        ]
        let seasonableAnimals = animals.compactMap { $0 as? Seasonable }
        print(seasonableAnimals)
        seasonableAnimals.forEach { $0.winterIsComing() }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        name = "Artem"
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
//            self?.performSegue(withIdentifier: "goToRedVC", sender: nil)
//        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print("viewWillLayoutSubviews")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goToRedVC" {
            if let destinationVC = segue.destination as? ViewController {
                _ = destinationVC.view // force load view
                destinationVC.mainButton.setTitle("\(elements.count) elements", for: .normal)
                // Мы буквально в контейнер ложим сам LoginViewController, что бы к нему потом обращаться но по упрощённому интерфейсу
                destinationVC.didUpdateElements = { [weak self] elements in
                    print("new elements count \(elements.count)")
                    self?.elements = elements
                }
//                destinationVC.delegate = self
                destinationVC.elements = elements
            }
        }
    }

    @IBAction func saveAction(_ sender: UIButton) {
        if let text = textField.text {
            elements.append(text)
            textField.text = ""
        }
    }
}

extension LoginViewController: ViewControllerDelegate {
    func elementsUpdated(elements: [String]) {
        print("new elements count \(elements.count)")
        self.elements = elements
    }
}
