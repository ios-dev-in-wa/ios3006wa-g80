//
//  BlackViewController.swift
//  ios3006ls6
//
//  Created by WA on 16.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class BlackViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func popToRootAction(_ sender: UIButton) {

        if let controller = storyboard?.instantiateViewController(withIdentifier: "WhiteViewController") {
            navigationController?.pushViewController(controller, animated: true)
        }
//        navigationController?.popToRootViewController(animated: true)
    }
}
