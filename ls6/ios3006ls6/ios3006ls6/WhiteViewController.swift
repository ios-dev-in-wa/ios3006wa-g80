//
//  WhiteViewController.swift
//  ios3006ls6
//
//  Created by WA on 16.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class WhiteViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("White", navigationController != nil)
        // Do any additional setup after loading the view.
    }

    @IBAction func trashAction(_ sender: UIButton) {
        if navigationController == nil {
            dismiss(animated: true) {
                print("White Controller dismissed")
            }
        } else {
            navigationController?.popToRootViewController(animated: true)
//            navigationController?.popViewController(animated: true)
        }
    }
}
