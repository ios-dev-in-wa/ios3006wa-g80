//
//  ViewController.swift
//  ios3006ls9
//
//  Created by WA on 28.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    let names: [String] = [
        "Artem",
        "Nikolay",
        "Roman",
        "Andrey"
    ]
    
    let userNameFileName = "user.txt"
    let imageFileName = "kitten.png"

    var fileNameUrl: URL? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        return url.appendingPathComponent(userNameFileName)
    }

    var imageUrl: URL? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        return url.appendingPathComponent(imageFileName)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        var data: Data!
//        data.write(to: <#T##URL#>)
        
//        saveArrayOfNames()
//        saveText()
//        readText()

//        saveImage()
//        readImage()
        setupUI()
//        dateExample()
//        asyncExample()
        
    }

    func asyncExample() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            print("I am async")
        }
        print("I am sync")
    }

    func setupUI() {
        segmentControl.removeAllSegments()
        MainColor.allCases.enumerated().forEach { index, value in
            segmentControl.insertSegment(withTitle: value.title, at: index, animated: false)
        }
        let appColor = Settings.shared.mainAppColor
        if let index = MainColor.allCases.firstIndex(of: appColor) {
            segmentControl.selectedSegmentIndex = index
        }
        view.backgroundColor = appColor.color
    }

    func saveImage() {
        let image = UIImage(named: "kitten")

        guard let data = image?.pngData(), let url = imageUrl else { return }
        do {
            try data.write(to: url)
        } catch {
            print(error.localizedDescription)
        }
    }

    func readImage() {
        guard let url = imageUrl else { return }

        do {
            let data = try Data(contentsOf: url)
            let image = UIImage(data: data)
            imageView.image = image
        } catch {
            print(error.localizedDescription)
        }
//        let image = UIImage()
    }

    func readText() {
        guard let url = fileNameUrl else { return }

        do {
            let value = try String(contentsOf: url, encoding: .utf8)
            print("userName was: \(value)")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func saveText() {
        let userName = "Artem"

        guard let url = fileNameUrl else { return }
        do {
            try userName.write(to: url, atomically: false, encoding: .utf8)
        } catch {
            print(error.localizedDescription)
        }
    }


    func saveArrayOfNames() {
        let nsArray = names as NSArray
        let urlString = "/Users/wa/Desktop/saved.txt"
        let urlToFile = URL(fileURLWithPath: urlString)
        nsArray.write(to: urlToFile, atomically: false)
    }

    func dateExample() {
        let currentDate = Date()
        print(currentDate.timeIntervalSince1970)
        print(currentDate)

        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d, yyyy"
        print(formatter.string(from: currentDate))

        print(formatter.date(from: "Tuesday, Jul 28, 2020"))
        let calendar = Calendar.current
        let components = calendar.dateComponents([.calendar, .day, .hour, .minute], from: currentDate)
        print(components.day, components.hour, components.minute)
    }

    @IBAction func didSelectColor(_ sender: UISegmentedControl) {
        let selectedColor = MainColor.allCases[sender.selectedSegmentIndex]
        view.backgroundColor = selectedColor.color
        Settings.shared.mainAppColor = selectedColor
    }
}

