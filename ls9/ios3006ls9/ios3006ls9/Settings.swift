//
//  Settings.swift
//  ios3006ls9
//
//  Created by WA on 28.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

enum MainColor: String, CaseIterable {
    case red, blue, black

    var color: UIColor {
        switch self {
        case .black: return .black
        case .red: return .red
        case .blue: return .blue
        }
    }

    var title: String {
        switch self {
        case .black: return "black"
        case .red: return "red"
        case .blue: return "blue"
        }
    }
}

class Settings {

    private init() { }

    static let shared = Settings()

    private let defaults = UserDefaults.standard

    var appStartCount: Int {
        get {
//            defaults.synchronize()
            return defaults.integer(forKey: "appStartCount")
        }
        set {
            defaults.set(newValue, forKey: "appStartCount")
        }
    }

    var mainAppColor: MainColor {
        get {
            let stringColor = defaults.string(forKey: "mainAppColor") ?? ""
            return MainColor(rawValue: stringColor) ?? .red
        }
        set {
            defaults.set(newValue.rawValue, forKey: "mainAppColor")
            defaults.synchronize()
        }
    }
    
}
