//
//  PickerViewController.swift
//  ios3006ls13
//
//  Created by WA on 13.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class PickerViewController: UIViewController {

    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    private var countries: [String] = {
        return NSLocale.isoCountryCodes.map { code in
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: Locale.current.languageCode ?? "").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            return name
        }
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.dataSource = self
        pickerView.delegate = self

        
        let current = Date()
        datePicker.minimumDate = current
        datePicker.maximumDate = current.addingTimeInterval(500000)
    }
}

extension PickerViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(countries[row])
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row]
    }
}

extension PickerViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    
}
