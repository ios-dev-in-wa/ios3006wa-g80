//
//  PageViewController.swift
//  ios3006ls13
//
//  Created by WA on 13.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

struct OnboardingModel {
    let imageName: String
    let title: String
    let subtitle: String
}

class PageViewController: UIPageViewController {

    let onboardingModels: [OnboardingModel] = [
        OnboardingModel(imageName: "imageOne", title: "Wow".localized, subtitle: "It is nice that you have joined us! Welcome on board!".localized),
        OnboardingModel(imageName: "imageTwo", title: "Try free premium :)", subtitle: "Give us your payment info and enjoy our app free for a week"),
        OnboardingModel(imageName: "imageThree", title: "Tell friends about us!", subtitle: "You can earn in-app coins for invite inviting your friends, do it easialy"),
//        OnboardingModel(imageName: "imageThree", title: "Tell friends about us!", subtitle: "You can earn in-app coins for invite inviting your friends, do it easialy,You can earn in-app coins for invite inviting your friends, do it easialy,You can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialyYou can earn in-app coins for invite inviting your friends, do it easialy")
    ]

    let pageControll = UIPageControl()
    var orderedControllers: [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        if let coloredVC = storyboard?.instantiateViewController(withIdentifier: "ColoredViewController") {
            view.addSubview(coloredVC.view)

            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                coloredVC.view.removeFromSuperview()
            }
        }
        delegate = self
        dataSource = self
//        print(Locale.current.regionCode, Locale.current.languageCode)
        var countries: [String] = []

        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: Locale.current.languageCode ?? "").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            countries.append(name)
        }

        print(countries)
        orderedControllers = onboardingModels.compactMap {
            self.createController(model: $0)
        }

        if let first = orderedControllers.first {
            setViewControllers([first], direction: .forward, animated: false, completion: nil)
        }

        pageControll.numberOfPages = orderedControllers.count
        pageControll.addTarget(self, action: #selector(didPressControl), for: .touchUpInside)
        view.addSubview(pageControll)

        pageControll.currentPageIndicatorTintColor = .black
        pageControll.pageIndicatorTintColor = .green
        pageControll.translatesAutoresizingMaskIntoConstraints = false

        pageControll.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageControll.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

    @objc func didPressControl(control: UIPageControl) {
        scrollToViewController(index: control.currentPage)
    }

    func createController(model: OnboardingModel) -> UIViewController? {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ColoredViewController") as? ViewController
        _ = controller?.view
        controller?.setupWith(model: model)

        return controller
    }

    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedControllers.firstIndex(of: firstViewController) {
            let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedControllers[newIndex]
            scrollToViewController(viewController: nextViewController, direction: direction)
        }
    }
    
    /**
     Scrolls to the given 'viewController' page.
     
     - parameter viewController: the view controller to show.
     */
    private func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewController.NavigationDirection = .forward) {
        setViewControllers([viewController],
            direction: direction,
            animated: true,
            completion: { (finished) -> Void in
                self.notifyTutorialDelegateOfNewIndex()
        })
    }

    private func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedControllers.firstIndex(of: firstViewController) {
            pageControll.currentPage = index
        }
    }
}

extension PageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        notifyTutorialDelegateOfNewIndex()
    }
}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = orderedControllers.firstIndex(of: viewController) else { return nil }
        if currentIndex == 0 {
            return nil
        }
        if currentIndex <= orderedControllers.count {
            let index = currentIndex - 1
            return orderedControllers[index]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = orderedControllers.firstIndex(of: viewController) else { return nil }
        if currentIndex == (orderedControllers.count - 1) {
            return nil
        }
        if currentIndex >= 0 {
            let index = currentIndex + 1
            return orderedControllers[index]
        }
        return nil
    }
}
