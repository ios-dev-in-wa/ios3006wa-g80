//
//  WebViewController.swift
//  ios3006ls13
//
//  Created by WA on 13.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let code = ""
//        webView.evaluateJavaScript(<#T##javaScriptString: String##String#>, completionHandler: <#T##((Any?, Error?) -> Void)?##((Any?, Error?) -> Void)?##(Any?, Error?) -> Void#>)
//        let request = URLRequest(url: URL(string: "https://www.google.com")!)
//        webView.load(request)

        if let url = Bundle.main.url(forResource: "videoExample", withExtension: "mov") {
            webView.loadFileURL(url, allowingReadAccessTo: url)
        }
    }
}
