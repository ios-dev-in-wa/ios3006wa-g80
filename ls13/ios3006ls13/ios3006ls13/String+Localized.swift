//
//  String+Localized.swift
//  ios3006ls13
//
//  Created by WA on 13.08.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
