//
//  SecondViewController.swift
//  ios3006ls7
//
//  Created by WA on 21.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import LocalAuthentication

class SecondViewController: UIViewController {

    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var movableView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var context = LAContext()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstButton.setTitle("Tit", for: .normal)
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let reason = "Log in to your account"
        context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in

            if success {

                // Move to the main thread because a state update triggers UI changes.
                DispatchQueue.main.async {
                     print("LOGGED IN")
                }

            } else {
                print(error?.localizedDescription ?? "Failed to authenticate")

                // Fall back to a asking for username and password.
                // ...
            }
        }
        imageView.image = UIImage(named: "51daf1998baf98c772d652f4cadd13f4")
        imageView.animationRepeatCount = 0
    }

    func moveView() {
        let x = CGFloat.random(in: 50...view.frame.maxX - 50)
        let y = CGFloat.random(in: 94...view.frame.maxY - 94)
        let newCenter = CGPoint(x: x, y: y)
        movableView.center = newCenter
        // to force close app
//        exit(0)
    }

    @IBAction func didTapView(_ sender: UITapGestureRecognizer) {
        moveView()
    }

    @IBAction func panAction(_ sender: UIPanGestureRecognizer) {
        if sender.state == .changed {
            let location = sender.location(in: view)
            movableView.center = location
        }
    }
    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
        case .changed:
            let rotation = sender.rotation
            movableView.transform = .init(rotationAngle: rotation)
            print(movableView.frame)
            print(movableView.bounds)
            print("–-––––––––––––––––")
        case .ended:
//            movableView.transform = .identity
            print()
        default:
            print("nothign")
        }
    }
}
