//
//  StackViewController.swift
//  ios3006ls7
//
//  Created by WA on 21.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class StackViewController: UIViewController {

    @IBOutlet weak var mainStackView: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func userDidTap(_ sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            print("Ended")
        case .possible:
            print("possible")
        case .began:
            print("began")
        case .changed:
            print("Changed")
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default:
            fatalError()
        }

    }
    @IBAction func didPanAction(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .ended:
            print("Ended")
        case .possible:
            print("possible")
        case .began:
            print("began")
        case .changed:
            let point = sender.location(in: view)
            if abs(sender.velocity(in: view).x) > 100 {
                sender.isEnabled = false
                sender.isEnabled = true
            }
            print(sender.translation(in: view))
//            print(point)
            if mainStackView.frame.contains(point) {
                print("U are not allowed to do it")
            }
            print("Changed")
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default:
            fatalError()
        }
    }

    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
                case .ended:
                    print("Ended")
                case .possible:
                    print("possible")
                case .began:
                    print("began")
                case .changed:
                    let point = sender.location(in: view)
                    print(sender.scale)
        //            print(point)
                    if mainStackView.frame.contains(point) {
                        print("U are not allowed to do it")
                    }
                    print("Changed")
                case .cancelled:
                    print("Cancelled")
                case .failed:
                    print("Failed")
                @unknown default:
                    fatalError()
                }
    }
    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
                case .ended:
                    print("Ended")
                case .possible:
                    print("possible")
                case .began:
                    print("began")
                case .changed:
                    let point = sender.location(in: view)
                    print(sender.rotation)
        //            print(point)
                    if mainStackView.frame.contains(point) {
                        print("U are not allowed to do it")
                    }
                    print("Changed")
                case .cancelled:
                    print("Cancelled")
                case .failed:
                    print("Failed")
                @unknown default:
                    fatalError()
                }
    }
}
