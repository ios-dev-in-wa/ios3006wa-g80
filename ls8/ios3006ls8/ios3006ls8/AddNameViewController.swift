//
//  AddNameViewController.swift
//  ios3006ls8
//
//  Created by WA on 23.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class AddNameViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!

    var didSaveName: ((String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }

    @IBAction func saveName(_ sender: UIButton) {
        guard let text = textField.text, !text.isEmpty else { return }
        didSaveName?(text)
        dismiss(animated: true, completion: nil)
    }
    
}

extension AddNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }
}
