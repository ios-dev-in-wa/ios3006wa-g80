//
//  CustomNameTableViewCell.swift
//  ios3006ls8
//
//  Created by WA on 23.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class CustomNameTableViewCell: UITableViewCell {

    @IBOutlet weak var nameValueLabel: UILabel!

    func setupWith(name: String) {
        nameValueLabel.text = name
    }
}
