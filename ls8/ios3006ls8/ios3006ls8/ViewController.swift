//
//  ViewController.swift
//  ios3006ls8
//
//  Created by WA on 23.07.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var sectionHeaders: [Character] {
        let firstElements = names.map({ name in name.uppercased() })
            .compactMap({ $0.first }).uniques
        return firstElements.sorted()
    }

    var rowsData: [Character: [String]] {
        var dictionary = [Character: [String]]()
        sectionHeaders.forEach { element in
            let filtered = names.filter { $0.first?.uppercased() == element.uppercased() }
            guard !filtered.isEmpty else { return }
            dictionary[element] = names.filter { $0.first?.uppercased() == element.uppercased() }
        }
        return dictionary
    }

    var names: [String] = [
        "Artem",
        "Sergey",
        "Ihor",
        "Illya",
        "kmfasmgljamkslgm;klasmdglkamsklgmakl;wemglqmsal;kgmksl;dmgklasmdkgan;"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        print(sectionHeaders)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "goToAddVC":
            guard let destinationVC = segue.destination as? AddNameViewController else { return }
            destinationVC.didSaveName = { [weak self] name in
                self?.names.append(name)
                self?.tableView.reloadData()
            }
        case "goToDetails":
            guard let destinationVC = segue.destination as? DetailViewController else { return }
            //        guard let indexPath = tableView.indexPathForSelectedRow else { return }
            guard let indexPath = sender as? IndexPath else { return }
            let key = sectionHeaders[indexPath.section]
            let elements = rowsData[key]
            let name = elements?[indexPath.row]
            _ = destinationVC.view
            destinationVC.nameLabel.text = name
        default:
            break
        }
//        guard segue.identifier == "goToDetails", let destinationVC = segue.destination as? DetailViewController else { return }
//        //        guard let indexPath = tableView.indexPathForSelectedRow else { return }
//                guard let indexPath = sender as? IndexPath else { return }
//                let key = sectionHeaders[indexPath.section]
//                let elements = rowsData[key]
//                let name = elements?[indexPath.row]
//                _ = destinationVC.view
//                destinationVC.nameLabel.text = name
    }
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected", indexPath)
        performSegue(withIdentifier: "goToDetails", sender: indexPath)
    }

//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        return .delete
//    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let actions = [UIContextualAction(style: .normal, title: "Save", handler: {_,_,_ in
            
        }), UIContextualAction(style: .destructive, title: "Delete", handler: { [weak self] _,_,_ in
            guard let self = self else { return }
            let key = self.sectionHeaders[indexPath.section]
            let elements = self.rowsData[key]
            let name = elements?[indexPath.row]
            self.names.removeAll(where: { $0 == name })
            self.tableView.reloadData()
        })]
        let configuration = UISwipeActionsConfiguration(actions: actions)
        return configuration
    }
}

extension ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool { return true }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return sectionHeaders.count
//    }

//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return String(sectionHeaders[section])
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return rowsData[sectionHeaders[section]]?.count ?? 0
        return names.count
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        print(indexPath)
//        let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell", for: indexPath)
////        let key = sectionHeaders[indexPath.section]
////        let elements = rowsData[key]
////        let name = elements?[indexPath.row]
//        let name = names[indexPath.row]
//        cell.textLabel?.text = name
////        cell.detailTextLabel
////        cell.imageView
//        return cell
//    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomNameTableViewCell", for: indexPath) as? CustomNameTableViewCell else { fatalError() }
        cell.setupWith(name: names[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceValue = names[sourceIndexPath.row]
        names.remove(at: sourceIndexPath.row)
        names.insert(sourceValue, at: destinationIndexPath.row)
        print(names)
    }
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
